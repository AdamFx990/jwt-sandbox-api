import jwt
import datetime
from flask_restful import Resource

secret = 'coconut'
algorithm = 'HS256'

class Login(Resource):
    def get(self):
        current_time = int(datetime.datetime.utcnow().timestamp())
        payload = {
            'email': 'adamfx990@protonmail.ch',
            'tenancy': 'myCoolTenancy',
            'issued': current_time
        }
        encoded_jwt = jwt.encode(payload, secret, algorithm)
        return {'access_token': encoded_jwt}, 200