# JWT Sandbox API
A sandbox JWT generator, Written in python using flask.

Returns a payload to the tune of:
`{ "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImFkYW1meDk5MEBwcm90b25tYWlsLmNoIiwidGVuYW5jeSI6Im15Q29vbFRlbmFuY3kiLCJpc3N1ZWQiOjE2NTYzMzEzODJ9.-tSuQIhTG7oEFFUBh9K1WI-HDyvE2y5MeHWVUAAH5Cc" }`

## Installation
Install python dependencies with:
`python -m pip install flask_restful pyjwt`

## Running the API
Run the API by executing main.py as follows:
`python main.py`